from random import randrange

m = [0]*25

# add bombs
for i in range(5):
  m[randrange(25)] = -1

# add tiles from bombs
for i in range(len(m)):
	if m[i] != -1:
		if 0 <= i - 1 < len(m) and m[i - 1] == -1: m[i] += 1 # w 
		if 0 <= i - 4 < len(m) and m[i - 4] == -1: m[i] += 1 # nw
		if 0 <= i - 5 < len(m) and m[i - 5] == -1: m[i] += 1 # n 
		if 0 <= i - 6 < len(m) and m[i - 6] == -1: m[i] += 1 # ne
		if 0 <= i + 1 < len(m) and m[i + 1] == -1: m[i] += 1 # e 
		if 0 <= i + 6 < len(m) and m[i + 6] == -1: m[i] += 1 # se
		if 0 <= i + 5 < len(m) and m[i + 5] == -1: m[i] += 1 # s 
		if 0 <= i + 4 < len(m) and m[i + 4] == -1: m[i] += 1 # sw

# hide all bombs and some tiles

print(m)
